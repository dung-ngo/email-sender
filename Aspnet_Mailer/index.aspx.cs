﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Aspnet_Mailer
{
    public partial class index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void sendBtn_Click(object sender, EventArgs e)
        {
                string from, pass, to, subject, body, server,name;
                int port;
                from = (fromTxt.Text).ToString();
                pass = (fromPassTxt.Text).ToString();
                to = (toTxt.Text).ToString();
                subject = (subjectTxt.Text).ToString();
                name = (nameTxt.Text).ToString();
            //body = (bodyTxt.Text).ToString();


            var header = System.IO.File.ReadAllText(Server.MapPath(@"~/View/header.txt"));
            var footer = System.IO.File.ReadAllText(Server.MapPath(@"~/View/footer.txt"));
            body = string.Format("Dear {0},Today {1}, thank you for registration! ",name,DateTime.Now);
 
                 server = (serverTxt.Text).ToString();
                port = int.Parse(portTxt.Text);
                MailMessage message = new MailMessage();
                message.From = new MailAddress(from);
                message.To.Add(to);
                message.Subject = subject;
                message.IsBodyHtml = true;
                message.Body = header +body + footer;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = server;
                smtp.Port = port;
                smtp.EnableSsl = true;
                smtp.UseDefaultCredentials = false;
                //smtp.Port = 587;
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.Credentials = new NetworkCredential(from, pass);
                try
                {
                    smtp.Send(message);
                    statusMsg.Text = "Mail has been sent successfully!";
                }
                catch (Exception ex)
                {
                    statusMsg.Text = ex.StackTrace;
                }


                //MailMessage message = new MailMessage(fromText.Text, toText.Text, subjText.Text, bodyText.Text);
                //message.IsBodyHtml = true;
                //SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
                //client.EnableSsl = true;
                //client.UseDefaultCredentials = false;
                //client.Credentials = new NetworkCredential(fromText.Text, "bluepl@net");
                //client.Send(message);
                //statusMsg.Text = "Mail has been sent successfully!";


        }
    }
}
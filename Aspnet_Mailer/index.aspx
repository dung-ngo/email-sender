﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="Aspnet_Mailer.index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Email Sender</title>
</head>
<body>
    <div class="container" align="center" width="100%" style="font-family: Arial, Helvetica, sans-serif; background: gray; color: white; margin: 0 400px; padding: 30px 80px">
        <h1 align="center">Email sender template</h1>
        <form id="form1" runat="server">
            <table align="center" width="100%">
                <tr>
                    <td>Sender Name</td>
                    <td colspan="2">
                        <asp:TextBox ID="nameTxt" runat="server" placeholder="your name" Width="80%"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>Sender</td>
                    <td colspan="2">
                        <asp:TextBox ID="fromTxt" runat="server" placeholder="your-email@mail.com" Width="80%"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>Sender Password</td>
                    <td colspan="2">
                        <asp:TextBox ID="fromPassTxt" TextMode="Password" runat="server" placeholder="your password" Width="80%"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>SMTP Server</td>
                    <td>
                        <asp:TextBox ID="serverTxt" runat="server" placeholder="your server" Width="80%"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>Port Number</td>
                    <td>
                        <asp:TextBox ID="portTxt" runat="server" placeholder="server port" Width="80%"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>Recipient</td>
                    <td>
                        <asp:TextBox ID="toTxt" runat="server" placeholder="recipient-email@mail.com" Width="80%"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>Subject</td>
                    <td>
                        <asp:TextBox ID="subjectTxt" runat="server" placeholder="your subject" Width="80%"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>Body</td>
                    <td>
                        <asp:TextBox ID="bodyTxt" runat="server" placeholder="add your content..." Width="80%" Height="100px" TextMode="MultiLine"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <asp:Button ID="sendBtn" OnClick="sendBtn_Click" runat="server" Text="Send" />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        
                        <small><asp:Label ID="statusMsg" runat="server" Width="60%"></asp:Label></small>
                    </td>
                </tr>
            </table>
        </form>
    </div>

</body>
</html>
